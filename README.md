# Final Year Project and Report and Zeug
<h3>Malaria Classification with CNN and ML</h3>
<p>It is a flask application but make sure you have followind versions of modules in virtual env</p>
<p>python 3.6.8</p>
<p>tensorflow 1.5.0</p>
<p>flask 1.1.2</p>
<p>jupyter 1.0.0</p>
<p>keras 2.2.4</p>
<p>matplotlib 3.2.1</p>
<p>numpy 1.18.5</p>
<p>opencv-python 4.2.0.34</p>
<p>pandas 1.0.4</p>
<p>pillow 7.1.2</p>
<p>scikit-learn 0.22.2</p>
<p>seaborn 0.10.1</p>
<p>In directory finalfl, run cmd and execute 'python index.py'</p>


