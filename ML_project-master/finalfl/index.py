from flask import Flask, render_template, request
from werkzeug.utils import secure_filename
import os
import numpy as np
from PIL import Image
import pickle
from keras.models import load_model
from keras.utils import to_categorical
from keras.preprocessing import image
import cv2


app = Flask(__name__)
app.config['IMAGE_UPLOADS'] = 'static'
global prob

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        imag = request.files['image']
        filename = secure_filename(imag.filename)
        imag.save(os.path.join(app.config["IMAGE_UPLOADS"], filename))
        status = True
        used = 'No'
        img = 'static/' + filename
        
        if request.form['model'] == 'cnn':
            idata = cv2.imread(img)
            idata = Image.fromarray(idata, 'RGB')
            idata = idata.resize((64, 64))
            idata = np.array(idata)
            idata = np.expand_dims(idata, axis=0)

            classify = load_model('model4.h5', compile=False)
            result = classify.predict(idata)
            print(result)
            result = result[0][0]
            inference = 'infected' if result>0.5 else 'uninfected'
            confidence = result if inference == 'infected' else (1 - result)

            return render_template('index.html', status = status, filename = filename, confidence = confidence, inf = inference, used = used)
        else:
            idata = image.load_img(img, target_size=(28, 28, 1), color_mode='grayscale')
            idata = image.img_to_array(idata)
            idata /= 255
            idata = np.array(idata)
            idata = idata.reshape(1, 28*28*1)
            
            if request.form['model'] == 'rf':
                model = 'RF.sav'   
            elif request.form['model'] == 'svm':
                model = 'SVM.sav'
            elif request.form['model'] == 'lr':
                model = 'LR.sav'
            elif request.form['model'] == 'knn':
                model = 'KNN.sav' 
            else: return render_template('index.html')

            classify = pickle.load(open(model, 'rb'))
            result = classify.predict_proba(idata)
            print(result)
            print(result[0][0])
            inference = 'infected' if result[0][0] >= 0.5 else 'uninfected'
            prob = result[0][0] if inference == 'infected' else 1 - result[0][0]
            used = "Logistic Regression" if request.form['model'] == 'lr' else ("Support Vector Machine" if request.form['model'] == 'svm' else ("K Nearest Neighbour" if request.form['model'] == 'knn' else "Random Forest"))
            
            return render_template('index.html', status = status, filename = filename, inf = inference, used = used, prob = prob)

    else:
        status = False
        return render_template('index.html')


if __name__ == '__main__':
    app.run(threaded=False)
